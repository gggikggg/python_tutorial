# รับค่าตัวเลขมาเก็บไว้ที่ตัวแปร num
num = int(input("Enter num: "))
# กำหนดให้ list มี 0-f 
num_base = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]

# กำหนดให้ x เท่ากับเลขฐานสิบหก(แปลงค่าจาก num)
for x in hex(num):
    # ถ้า x เท่ากับ 0
    if x == "0":
        # x จะเท่ากับ list ตัวที่ 0
        x = (num_base[0])
    # ถ้า x เท่ากับ 1
    if x == "1":
        # x จะเท่ากับ list ตัวที่ 0-1
        x = (num_base[0:1])
    # ถ้า x เท่ากับ 2
    if x == "2":
        # x จะเท่ากับ list ตัวที่ 0-2
        x = (num_base[0:3])
    # ถ้า x เท่ากับ 3
    if x == "3":
        # x จะเท่ากับ list ตัวที่ 0-3
        x = (num_base[0:4])
    # ถ้า x เท่ากับ 4
    if x == "4":
        # x จะเท่ากับ list ตัวที่ 0-4
        x = (num_base[0:5])
    # ถ้า x เท่ากับ 5
    if x == "5":
        # x จะเท่ากับ list ตัวที่ 0-5
        x = (num_base[0:6])
    # ถ้า x เท่ากับ 6
    if x == "6":
        # x จะเท่ากับ list ตัวที่ 0-6
        x = (num_base[0:7])
    # ถ้า x เท่ากับ 7
    if x == "7":
        # x จะเท่ากับ list ตัวที่ 0-7
        x = (num_base[0:8])
    # ถ้า x เท่ากับ 8
    if x == "8":
        # x จะเท่ากับ list ตัวที่ 0-8
        x = (num_base[0:9])
    # ถ้า x เท่ากับ 9
    if x == "9":
        # x จะเท่ากับ list ตัวที่ 0-9
        x = (num_base[0:-6])
    # ถ้า x เท่ากับ a
    if x == "a":
        # x จะเท่ากับ list ตัวที่ 0-(-5)
        x = (num_base[0:-5])
    # ถ้า x เท่ากับ b
    if x == "b":
        # x จะเท่ากับ list ตัวที่ 0-(-4)
        x = (num_base[0:-4])
    # ถ้า x เท่ากับ c
    if x == "c":
        # x จะเท่ากับ list ตัวที่ 0-(-3)
        x = (num_base[0:-3])
    # ถ้า x เท่ากับ d
    if x == "d":
        # x จะเท่ากับ list ตัวที่ 0-(-2)
        x = (num_base[0:-2])
    # ถ้า x เท่ากับ e
    if x == "e":
        # x จะเท่ากับ list ตัวที่ 0-(-1)
        x = (num_base[0:-1])
    # ถ้า x เท่ากับ f
    if x == "f":
        # x จะเท่ากับ list ตัวที่ 0-ตัวสุดท้าย
        x =(num_base[0:])

# แสดงค่า x 
print("Hexadecimal is: {x}".format(x=x))


