# รับค่าตัวเลขมาเก็บไว้ที่ตัวแปร num1
num1 = int(input("Enter num: "))
# รับค่าตัวเลขมาเก็บไว้ที่ตัวแปร num1
num2 = int(input("Enter num: "))

# กำหนดให้ num เท่ากับ 1 
num = 1
# ขณะที่ num น้อยกว่าหรือเท่ากับ num2
while num <= num2:
    # กำหนดให้ x เท่ากับ 1
    x = 1
    # ขณะที่ x น้อยกว่าหรือเท่ากับ num1
    while x <= num1:
        # แสดงค่า num1 num2 และ m(มาจากผลคูณของ x)
        print("Num : {num1}\nNum : {num2}\nResult : {m}\n".format(num1=num1, num2=num2, m=x*x))
        # เพื่มค่า x 1
        x += 1 
    # แสดงเพื่อเป็นตัวคั่น
    print("------")
     # เพื่มค่า num 1
    num += 1 


    
