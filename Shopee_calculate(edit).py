#รับ value ชื่อของลูกค้าแบบstring เก็บไว้ที่ variable customer 
customer = str(input("Enter Your Name :")) 

#รับ value ราคารองเท้าแบบint เก็บไว้ที่ variable shoes 
shoes = int(input("Enter cost of shoes :"))
#รับ value จำนวนรองเท้าแบบint เก็บไว้ที่ variable count_shoes
count_shoes = int(input("Enter count of shoes :"))

#รับ value ราคาเสื้อแบบint เก็บไว้ที่ variable shirts
shirts = int(input("Enter cost of shirts :"))
#รับ value จำนวนเสื้อแบบint เก็บไว้ที่ variable count_shirts
count_shirts = int(input("Enter count of shirts :"))

#รับ value ราคากางเกงแบบint เก็บไว้ที่ variable pants
pants = int(input("Enter cost of pants :"))
#รับ value จำนวนกางเกงแบบint เก็บไว้ที่ variable count_pants
count_pants = int(input("Enter count of pants :"))

#แสดง value ชื่อลูกค้า
print("Customer : {customer}".format(customer=customer))

#แสดง value ราคารองเท้า
print("Shoes : {shoes} Bath".format(shoes=shoes))
#แสดง value จำนวนรองเท้า
print("Count of shoes : {count_shoes}".format(count_shoes=count_shoes))

#ถ้าราคารองเท้ามากกว่าเท่ากับ 1000 
if shoes >= 3000: 
    #คำนวณเปอร์เซ็นต์เพื่อเป็นโปรโมชั่นส่วนลด
    promotion_shoes = 6 / 100 
    #คำนวณเปอร์เซ็นต์เหมือนกันแต่ค่าเป็น 6% 
    discount_shoes = 100 * 0.06
    #แสดงโปรโมชั่นส่วนลดที่ราคารองเท้าตรงกับเงื่อนไข 
    print("Discount shoes {discount_shoes} %".format(discount_shoes=discount_shoes))
#ถ้าราคารองเท้ามากกว่าเท่ากับ 2000
elif shoes >= 2000: 
    #คำนวณเปอร์เซ็นต์เพื่อเป็นโปรโมชั่นส่วนลด
    promotion_shoes = 4 / 100 
    #คำนวณเปอร์เซ็นต์เหมือนกันแต่ค่าเป็น 4% 
    discount_shoes = 100 * 0.04 
    #แสดงโปรโมชั่นส่วนลดที่ราคารองเท้าตรงกับเงื่อนไข 
    print("Discount shoes {discount_shoes} %".format(discount_shoes=discount_shoes))
#ถ้าราคารองเท้ามากกว่าเท่ากับ 3000
elif shoes >= 1000:
    #คำนวณเปอร์เซ็นต์เพื่อเป็นโปรโมชั่นส่วนลด
    promotion_shoes = 2 / 100
    #คำนวณเปอร์เซ็นต์เหมือนกันแต่ค่าเป็น 2%  
    discount_shoes = 100 * 0.02
    #แสดงโปรโมชั่นส่วนลดที่ราคารองเท้าตรงกับเงื่อนไข 
    print("Discount shoes {discount_shoes} %".format(discount_shoes=discount_shoes))
else:
    #นอกเงื่อนไขกำหนดให้เท่ากับ 0
    promotion_shoes = 0
    #แสดงข้อความว่าไม่ได้รับส่วนลด
    print("Did not get a discount!!")

#คำนวณราคารองเท้าและจำนวนรองเท้ากับโปรโมชั่นส่วนสด ที่ใช้คำนวณเปอร์เซ็นต์สองอัน เพราะว่าเมื่อเอา 100*0.02 แล้วคำนวณไม่ถูกและเพราะเข้าใจแบบนี้มากกว่า
total_shoes = (shoes * count_shoes) - (promotion_shoes * shoes)
#แสดงราคาที่ต้องจ่ายของรองเท้า
print("Total shoes : {total_shoes} Bath".format(total_shoes=total_shoes))

#แสดง value ราคาเสื้อ
print("Shirts : {shirts} Bath".format(shirts=shirts))
# แสดง value จำนวนเสื้อ
print("Count of shirts : {count_shirts}".format(count_shirts=count_shirts))

#ถ้าราคาเสื้อมากกว่าเท่ากับ 1000  
if shirts >= 2000: 
    #คำนวณเปอร์เซ็นต์เพื่อเป็นโปรโมชั่นส่วนลด
    promotion_shirt = 4 / 100
    #คำนวณเปอร์เซ็นต์เหมือนกันแต่ค่าเป็น 4%  
    discount_shirt = 100 * 0.04 
    #แสดงโปรโมชั่นส่วนลดที่ราคาเสื้อตรงกับเงื่อนไข 
    print("Discount {discount_shirt} %".format(discount_shirt=discount_shirt))
    #ถ้าราคาเสื้อมากกว่าเท่ากับ 2000
elif shirts >= 1000:  
    #คำนวณเปอร์เซ็นต์เพื่อเป็นโปรโมชั่นส่วนลด
    promotion_shirt = 3 / 100 
    #คำนวณเปอร์เซ็นต์เหมือนกันแต่ค่าเป็น 3%
    discount_shirt = 100 * 0.03  
    #แสดงโปรโมชั่นส่วนลดที่ราคาเสื้อตรงกับเงื่อนไข 
    print("Discount {discount_shirt} %".format(discount_shirt=discount_shirt))
else:
    #นอกเงื่อนไขกำหนดให้เท่ากับ 0
    promotion_shirt = 0
    #แสดงข้อความว่าไม่ได้รับส่วนลด
    print("Did not get a discount!!")
#คำนวณราคาเสื้อและจำนวนเสื้อกับโปรโมชั่นส่วนสด
total_shirts = (shirts * count_shirts) - (promotion_shirt * shirts) 
#แสดงราคาที่ต้องจ่ายของเสื้อ
print("Total shirths : {total_shirts} Bath".format(total_shirts=total_shirts))

#แสดง value ราคากางเกง
print("Pants : {pants} Bath".format(pants=pants))
#แสดง value จำนวนกางเกง
print("Count of pants : {count_pants}".format(count_pants=count_pants))

#ถ้าราคากางเกงมากกว่าเท่ากับ 500
if pants >= 1000: 
    #คำนวณเปอร์เซ็นต์เพื่อนำเป็นโปรโมชั่นส่วนลด
    promotion_pants = 10 / 100 
    #คำนวณเปอร์เซ็นต์เหมือนกันแต่ค่าเป็น 10% 
    discount_pants = 100 * 0.10
    print("Discount {discount_pants} %".format(discount_pants=discount_pants))
    #ถ้าราคากางเกงมากกว่าเท่ากับ 1000
elif pants >= 500:
    #คำนวณเปอร์เซ็นต์เพื่อเป็นโปรโมชั่นส่วนลด 
    promotion_pants = 5 / 100 
    #คำนวณเปอร์เซ็นต์เหมือนกันแต่ค่าเป็น 5% 
    discount_pants = 100 * 0.05
    #แสดงโปรโมชั่นส่วนลดที่ราคากางเกงตรงกับเงื่อนไข 
    print("Discount {discount_pants} %".format(discount_pants=discount_pants))
else:
    #นอกเงื่อนไขกำหนดให้เท่ากับ 0
    promotion_pants = 0
    #แสดงข้อความว่าไม่ได้รับส่วนลด
    print("Did not get a discount!!")

#คำนวณราคากางเกงและจำนวนกางเกงกับโปนโมชั่นส่วนสด
total_pants = (pants * count_pants) - (promotion_pants * pants)
#แสดงราคาที่ต้องจ่ายของกางเกง
print("Total pants : {total_pants} Bath".format(total_pants=total_pants))

#คำนวณราคาของสิ้นค้าทั้งหมด
total_discounts = total_shoes + total_shirts + total_pants 

#ถ้าราคาสิ้นค้าทั้งหมดมากกว่าหรือเท่ากับ 5000
if total_discounts >= 5000: 
    #ได้ส่วนลดจากshopee 500
    promotion_shopee = 500 
    #แสดงส่วนลดที่ได้รับจาก shopee
    print("Shopee discount {promotion_shopee} Bath".format(promotion_shopee=promotion_shopee))
    #ถ้าราคาสิ้นค้าทั้งหมดน้อยกว่า 5000
elif total_discounts < 5000: 
    #ไม่ได้ส่วนลดจากshopee
    promotion_shopee = 0 
    #แสดงข้อความว่าไม่ได้รับส่วนลด
    print("Did not get a discount!!")

#คำนวณราคาสิ้นค้าทั้งหมดกับส่วนลดshopee
discount_shopee = total_discounts - promotion_shopee 

#ราคาสิ้นค้าที่ต้องจ่ายทั้งสิ้น
total = discount_shopee
#แสดงราคาสิ้นค้าที่ต้องจ่ายทั้งสิ้น
print("Total : {total} Bath".format(total=total))

#รับ value จำนวนเงินที่จ่าย เก็บไว้ที่ variable received
received = int(input("Amount Received : "))
#คำนวณจำนวนเงินที่จ่ายกับราคาสิ้นค้าที่ต้องจ่าย
remainders = received - total
#แสดงจำนวนเงินคงเหลือ
print("Amount Remainders {remainders} Bath".format(remainders=remainders))

