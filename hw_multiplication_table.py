# กำหนดให้ row มีค่าเท่ากับ 1
row = 1
# รับค่าตัวเลข
num = int(input("Enter number: "))

# ขณะที่ row มากกว่า 12 
while row < 12:
    # กำหนดให้ x มีค่าเท่ากับ 1 (ตัวที่จะนำไปคูณกับค่าnum)
    x = 1
    # ขณะที่ x มากกว่า 13
    while x < 13:
        # แสดงค่า num * x = m(มาจาก num*x)
        print("{num} * {x} = {m}".format(num=num, x=x, m=num*x))
        # เพิ่มค่า col 1
        x += 1
    # แสดงเพื่อเป็นตัวคั่น
    print("---------------")
    # เพิ่มค่า num 1
    num += 1
    # เพิ่มค่า row 1
    row += 1
 